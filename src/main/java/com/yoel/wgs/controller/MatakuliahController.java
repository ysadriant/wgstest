package com.yoel.wgs.controller;

import com.yoel.wgs.dao.MatakuliahDao;
import com.yoel.wgs.entity.Matakuliah;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Yoel on 7/24/2019.
 */
@RestController
public class MatakuliahController {
    @Autowired
    private MatakuliahDao matakuliahDao;

    @RequestMapping(value = "/api/matakuliah", method = RequestMethod.GET)
    public Page<Matakuliah> findMatakuliahAll(Pageable page) {
        return matakuliahDao.findAll(page);
    }

    @RequestMapping(value = "/api/matakuliah", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void insertMatakuliah(@RequestBody Matakuliah matakuliah) {
        matakuliahDao.save(matakuliah);
    }

    @RequestMapping(value = "/api/matakuliah/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void updateMatakuliah(@PathVariable("id") Long id, @RequestBody Matakuliah matakuliah) {
        matakuliah.setId(id);
        matakuliahDao.save(matakuliah);
    }

    @RequestMapping(value = "/api/matakuliah/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Matakuliah findMatakuliahById(@PathVariable("id") Long id) {
        return matakuliahDao.findOne(id);
    }

    @RequestMapping(value = "/api/matakuliah/delete/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void deleteMatakuliah(@PathVariable("id") Long id) {
        matakuliahDao.delete(id);
    }
}
