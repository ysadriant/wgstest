package com.yoel.wgs.controller;

import com.yoel.wgs.dao.MahasiswaDao;
import com.yoel.wgs.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Yoel on 7/24/2019.
 */
@RestController
public class MahasiswaController {
    @Autowired
    private MahasiswaDao mahasiswaDao;

    @RequestMapping(value = "/api/mahasiswa", method = RequestMethod.GET)
    public Page<Mahasiswa> findMahasiswaAll(Pageable page){
        return mahasiswaDao.findAll(page);
    }

    @RequestMapping(value = "/api/mahasiswa", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void insertMahasiswa(@RequestBody Mahasiswa mahasiswa){
        mahasiswaDao.save(mahasiswa);
    }

    @RequestMapping(value = "/api/mahasiswa/{id}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void updateMahasiswa(@PathVariable("id") Long id, @RequestBody Mahasiswa mahasiswa) {
        mahasiswa.setId(id);
        mahasiswaDao.save(mahasiswa);
    }

    @RequestMapping(value = "/api/mahasiswa/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Mahasiswa findMahasiswaById(@PathVariable("id") Long id) {
        return mahasiswaDao.findOne(id);
    }

    @RequestMapping(value = "/api/mahasiswa/delete/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void deleteMahasiswa(@PathVariable("id") Long id){
        mahasiswaDao.delete(id);
    }


}
