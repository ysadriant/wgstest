package com.yoel.wgs.dao;

import com.yoel.wgs.entity.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Yoel on 7/24/2019.
 */
public interface MatakuliahDao extends JpaRepository<Matakuliah, Long> {
}
