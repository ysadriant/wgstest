package com.yoel.wgs.dao;

import com.yoel.wgs.entity.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Yoel on 7/24/2019.
 */
public interface MahasiswaDao extends JpaRepository<Mahasiswa, Long> {
}
