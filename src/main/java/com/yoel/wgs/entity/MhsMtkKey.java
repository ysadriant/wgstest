package com.yoel.wgs.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Yoel on 7/24/2019.
 */
@Embeddable
public class MhsMtkKey implements Serializable {
    @Column(name = "mhs_id")
    Long mahasiswaId;

    @Column(name = "mtk_id")
    Long matakuliahId;

    public Long getMahasiswaId() {
        return mahasiswaId;
    }

    public void setMahasiswaId(Long mahasiswaId) {
        this.mahasiswaId = mahasiswaId;
    }

    public Long getMatakuliahId() {
        return matakuliahId;
    }

    public void setMatakuliahId(Long matakuliahId) {
        this.matakuliahId = matakuliahId;
    }
}
