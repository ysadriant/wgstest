package com.yoel.wgs.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Yoel on 7/24/2019.
 */
@Entity
public class MhsMtk {

    @EmbeddedId
    MhsMtkKey id;

    @ManyToOne
    @MapsId("mhs_id")
    @JoinColumn(name = "mhs_id")
    Mahasiswa mahasiswa;

    @ManyToOne
    @MapsId("mtk_id")
    @JoinColumn(name = "mtk_id")
    Matakuliah matakuliah;


    public MhsMtkKey getId() {
        return id;
    }

    public void setId(MhsMtkKey id) {
        this.id = id;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }
}
