package com.yoel.wgs.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yoel on 7/24/2019.
 */
@Entity
@Table(name = "mahasiswa")
public class Mahasiswa {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "nama")
    private String name;

    @ManyToMany
    @JoinTable(name = "mhs_mk_join", joinColumns = {@JoinColumn(name = "mhs_id")}, inverseJoinColumns = {@JoinColumn(name = "mtk_id")})
    private Set<Matakuliah> matakuliah;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
